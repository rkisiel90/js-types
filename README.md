# pure-js-types

My approach to get control over JavaScript types without static typed languages and transpilers.

Main motivation was to avoid problems with ```typeof``` operator in JS like:

```js
  typeof [] // => object
```

and simplyfy typechecking when assigning the value to variable like:

```js
  function Something(sth) {
    this.isHandy = (typeof sth === 'boolean') ? sth : false;
  }
```

or just enhance the code quality by taking care of types.

## Getting started

```shell
yarn install
```

or

```shell
npm install
```

## Usage

### **Get type of some value:**

```js
xt.type.get('Lets check it') // => "string"
xt.type.get([1,2,3,4]) // => "array"
```

### **Check if value is in desired type:**

```js
xt.type.is([1,2,3,4], "array") // => true
```

### **Assign value to variable if fits to type:**

```js
var isItSth = xt.type.pick(false, 'boolean', true) // => false
var isItToo = xt.type.pick({sth: 1}, 'array', [1,2,3]) // => [1,2,3]
```

## Example

For trivial example see [script](src/js/script.js)

## Tests

To start tests:

```shell
yarn run test
```

To get coverage report:

```shell
yarn run test --coverage
```

## Notes

Module has been created without ES6 because project where I have been using it is old codebase.