/*jshint esversion: 6 */
/*jshint node: true */
/* globals describe, test, expect */

beforeEach(() => {
  global.console = {
    warn: jest.fn()
  }
});

require('../xt.type');

var samples = {
  'array': [1,2,3],
  'object': {sth: 1},
  'string': 'sth',
  'date': new Date(),
  'regexp': /ab+c/,
  'function': function(){},
  'boolean': true,
  'number': 1,
  'null': null,
  'undefined': undefined
};
var samplesKeys = Object.keys(samples);

describe('get - check if gets correct type of passed value', function () {
  samplesKeys.forEach(function (sampleKey, index) {

    test(sampleKey, function () {
      var example = samples[sampleKey];
      var resultOfTypeGet = xt.type.get(example);

      expect(resultOfTypeGet).toBe(sampleKey);
    });
  });
});

describe('is - check if passed value is/is not in expected type', function () {
  describe('confirms that', function() {
    samplesKeys.forEach(function (sampleKey, index) {
      var example = samples[sampleKey];
      var resultOfTypeGet = xt.type.is(example, sampleKey);

      test(sampleKey + ' is ' + sampleKey, function () {
        expect(resultOfTypeGet).toBe(true);
      });
    });
  });
  describe('confirms that', function() {
    samplesKeys.forEach(function (sampleKey, index) {
      var nextSampleKey = (index + 1 === samplesKeys.length) ? samplesKeys[0] : samplesKeys[index + 1];
      var example = samples[sampleKey];
      var resultOfTypeGet = xt.type.is(example, nextSampleKey);

      test(sampleKey +' isn\'t ' + nextSampleKey, function () {
        expect(resultOfTypeGet).toBe(false);
      });
    });
  });
});

describe('pick - check if picks the provided value when it fits to the expected type.', function () {
  describe('when passed value matches expected type it should return it', function () {
    samplesKeys.forEach(function(sampleKey, index) {
      var nextSampleKey = (index+1 === samplesKeys.length) ? samplesKeys[0] : samplesKeys[index+1];

      test(sampleKey, function () {
        var example = samples[sampleKey];
        var resultOfTypePick = xt.type.pick(example, sampleKey, samples[nextSampleKey]);

        expect(resultOfTypePick).toBe(example);
      });
    });
  });

  describe('when passed value doesn\'t match expected type it should return default value', function () {
    samplesKeys.forEach(function (sampleKey, index) {
      var nextSampleKey = (index + 1 === samplesKeys.length) ? samplesKeys[0] : samplesKeys[index + 1];

      test(sampleKey, function () {
        var example = samples[nextSampleKey];
        var expectedType = sampleKey;
        var defaultValue = samples[sampleKey];
        var resultOfTypePick = xt.type.pick(example, expectedType, defaultValue);


        expect(resultOfTypePick).toBe(defaultValue);
        expect(global.console.warn).toHaveBeenCalledWith('Provided value: ' + example + ' doesn\'t match the expected type: ' + expectedType + '. Default value has been used')
      });
    });
  });
});