(function () {
  'use strict';

  function Person(firstName, canDance) {

    this.firstName = xt.type.pick(firstName, 'string', 'Tom');
    this.canDance = xt.type.pick(canDance, 'boolean', true); // normally people can dance ;)
  }

  var me = new Person('Robert', false); // whoops I'm not the best dancer! ;)


}());