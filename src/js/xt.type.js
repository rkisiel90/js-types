window.xt = window.xt || {};
window.xt.type = window.xt.type || {};

(function( type, window, document, undefined ) {
	'use strict';

	// ****************************************************
	// Public Methods
	// ****************************************************

	/**
	* Returns real type of passed value
	* @see https://toddmotto.com/understanding-javascript-types-and-reliable-type-checking/
	*
	* @param {number} value any value which type you want to determinate
	* @returns {string} string representing type of passed value
	*
	* @author Robert Kisiel
	*/
	type.get = function(value) {
		return Object.prototype.toString.call(value).slice(8, -1).toLowerCase();
	};

	/**
	* Checks if proided value is in provided type
	*
	* @param {*} value any value which type you want to determinate
	* @param {string} expectedType string representing expecting type of passed value
	* @returns {boolean}
	*
	* @author Robert Kisiel
	*
	*/
	type.is = function(value, expectedType) {

		return type.get(value) === expectedType;
	};

	/**
	* Picks the provided value if fits to expected type. Otherwise returns default value.
	*
	* @param {*} value - any value which type you want to determinate
	* @param {string} expectedType - string representing expecting type of passed value
	* @param {*} defaultValue - fallback if value doesn't match expect type
	*
	* @returns {*} Passed value or default value.
	*
	* @author Robert Kisiel
	*
	*/
	type.pick = function (value, expectedType, defaultValue) {
		var isInType = type.is(value, expectedType);

		if (isInType) {
			return value;
		} else {
			console.warn('Provided value: ' + value + ' doesn\'t match the expected type: ' + expectedType + '. Default value has been used');
			return defaultValue;
		}
	};

}( xt.type = xt.type || {}, window, document ));